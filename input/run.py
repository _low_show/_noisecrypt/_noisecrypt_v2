from flask import Flask, Response
import requests
from sys import getsizeof
from random import randint
import subprocess
import atexit
import os

app = Flask(__name__)

# TODO:
# Start icecast (set port)
# get input streams (via jack?)
# encode stream(s) with opusenc (set bitrate 64 and hard-cbr and downmix-mono) forward to icecast
# optionally send stream to local file as well
# set port for proxy server
# set host(s) and send open request to host
# log requests
# kill icecast, streams (and server?) on end (close file writing)
# TODO: send version header with requests


@app.route('/')
def streamed_response():

    r = requests.get('http://127.0.0.1:1234/noisecrypt.opus', stream=True)

    def generate():

        total_size = 0
        # we offset by 1 because 1 seconds were failing
        # (presumably due to some encoding data)
        seconds = randint(2, 11)
        # at 64kbps it's 8 KB per second
        max_size = 1024 * 8 * seconds
        for chunk in r.iter_content(chunk_size=1024):

            if chunk:

                chunk_size = getsizeof(chunk)
                total_size = total_size + chunk_size
                yield chunk

            if total_size >= max_size:

                break

    excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
    headers = [(name, value) for (name, value) in r.raw.headers.items() if name.lower() not in excluded_headers]
    return Response(generate(), r.status_code, headers)


def main():

    # TODO: handle start/kill of icecast via subprocess

    # Run flask web server
    app.run(threaded=True)


if __name__ == '__main__':

    main()
