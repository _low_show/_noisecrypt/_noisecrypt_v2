#!/bin/bash

# Prevent running in case of failures
set -euf -o pipefail

# Ensure system and packages are up to date
sudo apt-get update -yq
sudo apt-get upgrade -yq

# Remove unnecessary files
sudo apt autoremove -yq

# Install dependencies
sudo DEBIAN_FRONTEND=noninteractive apt-get install \
    git opus-tools jackd2 python3-pyo libsndfile1-dev \
    portaudio19-dev python-pyaudio python3-pyaudio \
    libjack-jackd2-dev libportmidi-dev liblo-dev \
    python3-pip nginx -yq

# Add extra binaries to path
# shellcheck disable=SC1090
echo "export PATH=\$PATH:\$HOME/.local/bin:" >> ~/.profile
# shellcheck disable=SC1090
source ~/.profile

# If no pyo, clone, if pyo, remove build
if ! test -d "pyo"; then

    git clone https://github.com/belangeo/pyo.git
    cd pyo

else

    cd pyo
    rm -rf build
    git pull

fi

# Build pyo
sudo python3 setup.py install --use-jack --use-double
cd ..

# Install venv
pip3 install virtualenv -q

# Create virtual environment
virtualenv -q venv
if test -d "venv"; then
    # shellcheck disable=SC1091
    source "venv/bin/activate"

else

    echo "Virtual environment was not created."
    exit 1
    
fi

# Install python dependencies
pip install ./pyo pyalsaaudio pick requests tinytag pyo flask uwsgi

# exit venv
deactivate
