#!/bin/bash

# Prevent running in case of failures
set -euf -o pipefail

# Run the python app
../venv/bin/python setup.py

