#!/bin/python3

from alsaaudio import cards, PCM_PLAYBACK, Mixer
from pick import pick
import os
from typing import List
import logging


def boost_audio(indices: List[int]):
    """
    Set USB audio devices to have maximum volume.
    """

    # Assuming we have stereo out and mono in
    try:

        for device in indices:

            m = Mixer('Speaker', 0, device)
            m.setvolume(100, 0)
            m.setvolume(100, 1)
            c = Mixer('Capture', 0, device)
            c.setVolume(100, 0)

    except Exception:

        logging.warning("Failed to set channel volume")


def write_conf_content(file_name: str, content: str):
    """
    Write conf data to file
    """

    with open(file_name, "w") as f:

        f.write(content)
        f.close()


def get_conf_content(indices: List[int]) -> str:
    """
    Writes configuration for virtual device to conf
    """

    num = len(indices)
    space = "   "

    content = [
        'pcm.pyo_jack_alsa {',
        '{}type multi;'.format(space)
    ]

    # For now, we are only dealing with 4 cards max
    slaves = ["a", "b", "c", "d"]

    for index, char in slaves:

        if index == num:
            break

        line1 = "{}slaves.{}.pcm \"hw:{},0\";".format(
            space, char, indices[index])
        content.append(line1)

        line2 = "{}slaves.{}.channels 2;".format(space, char)
        content.append(line2)

    for index, char in slaves:

        if index == num:
            break

        n = index * 2
        lines = [
            "{}bindings.{}.slave {};".format(space, n, char),
            "{}bindings.{}.channel 0;".format(space, n, char),
            "{}bindings.{}.slave {};".format(space, n + 1, char),
            "{}bindings.{}.channel 1;".format(space, n + 1, char)
        ]

        for line in lines:
            content.append(line)

    block1 = [
        "}\n",
        "ctl.pyo_jack_alsa {",
        "{}type hw;".format(space),
        "{}card 0;".format(space),
        "}\n",
        "pcm.pyo_jack {",
        "{}type route;".format(space),
        "{}slave.pcm \"pyo_jack_alsa\";".format(space),
        "{}slave.channels {};".format(space, num)
    ]

    for line in block1:
        content.append(line)

    for index, _ in enumerate(indices):
        content.append("{}ttable.{}.{} 1;".format(space, index, index))

    block2 = [
        "}\n"
        "{}ctl.pyo_jack {".format(space),
        "{}type hw;".format(space),
        "{}card 0;".format(space),
        "}"
    ]

    for line in block2:
        content.append(line)

    return "\n".join(content)


def setup_conf() -> str:
    """
    Ensure the conf file exists and it is referenced
    from the main asoundrc file.
    """

    local_conf = os.path.join(os.path.abspath(__file__), 'pyo_jack.conf')
    conf_ref = "<{}>".format(local_conf)

    # check for asoundrc, create, add conf file name
    asrc_file = os.path.join(os.path.expanduser('~'), '.asoundrc')
    with open(asrc_file, 'a+') as f:

        f.seek(0)
        for line in f:

            if conf_ref in line:
                break

        else:  # not found

            f.write(conf_ref)

        f.close()

    return local_conf


def get_device_indices() -> List[int]:
    """
    Uses user interaction to select the device
    indices to be used when setting up the virtual
    device for jack.
    """

    options = []
    card_items = []

    # find usb devices
    for device_number, card_name in enumerate(cards()):

        options.append("{}: {}".format(device_number, card_name))
        card_items.append(device_number)

    title = (
        'Please select the USB devices for your output.\n'
        'Press SPACE to mark, ENTER to continue.'
    )

    selected = pick(options, title, multiselect=True, min_selection_count=1)

    selected_ids = []

    # create list of selected device indices
    for item in selected:

        option, index = item
        selected_ids.append(card_items[index])

    return selected_ids


def setup():
    """
    Run procedure for setting up the devices with jack.
    """

    # Retrieve device IDs to use
    selected_ids = get_device_indices()

    # Setup conf files, get file name
    conf = setup_conf()

    # write conf data
    content = get_conf_content(selected_ids)
    write_conf_content(conf, content)

    # Boost audio device channels
    boost_audio(selected_ids)


if __name__ == '__main__':

    setup()
