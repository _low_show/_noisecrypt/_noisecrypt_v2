import subprocess
from time import sleep

def is_jack_running() -> bool:
    """
    Uses jack process listing to retrieve an error code.
    :return: boolean
    """

    command = 'jack_lsp 1>/dev/null 2>&1'

    status = subprocess.getstatusoutput(command)

    return status[0] == 0

def get_channel_count() -> int:
    """
    Uses configuration file to retrieve channel count.
    :return: channel count integer
    """

    with open('pyo_jack.conf', 'r') as f:

        for line in f.readlines():

            if "slave.channels" in line:

                num = [int(s) for s in line.split() if s.isdigit()]
                return num[0]

    return 0

def kill():
    """
    Kills the jack server if it is running.
    """

    if not is_jack_running():

        print("Jack is not running.")
        return

    subprocess.getstatusoutput("killall jackd")

def start():
    """
    Start's the jack server if it isn't running.
    Uses channel count from configuration file.
    """

    if is_jack_running():

        print("Jack is already running.")
        return

    channels = get_channel_count()

    if channels is 0:

        print("No channels.")
        return

    command = ('jackd -r -p16 -t2000 -d alsa '
               '-d pyo_jack -p 1024 -n3 -r 44100 '
               '-S -o {} 1>/dev/null 2>&1 &'.format(channels))

    subprocess.getstatusoutput(command)

def speaker_test():
    """
    Runs a speaker test on each channel if jack server is running.
    """

    if not is_jack_running():

        print("Jack is not running.")
        return

    channels = get_channel_count()

    if channels is 0:

        print("No channels.")
        return

    # start metronome
    subprocess.getstatusoutput("jack_metro - b60 &")

    print("A beeping sound will be emitted by each channel individually.")

    # Test each speaker channel
    for channel in range(channels):

        print("Testing channel {}".format(channel))
        subprocess.getstatusoutput("jack_connect metro:60_bpm system:playback_{}".format(channel))
        sleep(3)
        subprocess.getstatusoutput("jack_disconnect metro:60_bpm system:playback_{}".format(channel))
        print("End testing channel {}".format(channel))
        sleep(1)

    # Kill processes
    subprocess.getstatusoutput("pkill - 9")
    subprocess.getstatusoutput("jack_metro")
